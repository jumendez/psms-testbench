from testResLib import TestResLib
from testResDecorator import TestResDecorator
import random
import time
import datetime

@TestResDecorator
def subtest_instance(channel=0):
    t0 = time.time()

    measurements = []
    vals = []

    #Subtest details
    details = {
        'channel': channel,
        'date': datetime.datetime.now()
    }

    #Measurements
    for i in range(10):
        val = np.random.normal(512,10,1)[0]
        vals.append(val)
        measurements.append({
                'name': 'measname',
                'datapoint': {'i': i, 'val': val},
                'passed': 1
            })

    #Analyze
    errors = False

    min = min(vals)
    max = max(vals)
    mean = mean(vals)
    std = std(vals)

    if min < 0 or max > 1024:
        errors = True

    #Summaryze
    summary = {
            'count': len(measurements),
            'min': min,
            'max': max,
            'mean':mean,
            'std':std,
            'test_time':time.time()-t0
        }

    return {
        'name': 'FEATURETEST_{}'.format(gain,channel),
        'summary': summary,
        'measurements': measurements,
        'details': details,
        'passed': (TestResLib.SUCCESS if errors == False else TestResLib.FAILED)}

if __name__ == "__main__":
    username = 'to_be_set'
    password = 'to_be_set'
    host = 'to_be_set'
    port = 0000
    dbname = 'to_be_set'

    start = datetime.datetime.now()

    testRes = TestResLib(host,username,password,port,dbname)
    deviceId = testRes.addDevice(deviceName = 'deviceName', deviceDesc = {'ex': 'test'})
    testId = testRes.addTest(deviceId = deviceId, testDescription = {'testbench':'v.0.1', 'operator':'xxx'})

    for channel in range(5):
        subtest_instance(
                            testRes=testRes,
                            testId = testId,
                            deviceId=deviceId,
                            testTypeId=testTypeId,

                            channel=channel
                        )

    print('Test done in {} seconds (testId = {})'.format((datetime.datetime.now()-start).total_seconds(), testId))