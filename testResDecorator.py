#!/usr/bin/env python2
#-*- coding: utf-8 -*-
import time
from testResLib import TestResLib

class TestResDecorator:

    ''' Init: call the connect function '''
    def __init__(self, test_function):
        """ Constructor, nothing special here"""
        self.test_function = test_function

    def __call__(self,
                 testRes=None,            # (optional)
                 host=None,               #   -> To be defined if testRes = None
                 username=None,           #   -> To be defined if testRes = None
                 password=None,           #   -> To be defined if testRes = None
                 port=None,               #   -> To be defined if testRes = None
                 dbname=None,             #   -> To be defined if testRes = None

                 testId=None,             # (optional)
                 testDesc = None,         #   -> To be defined if testId = None

                 deviceId=None,           # (optional)
                 searchDeviceName=None,   #   -> To be defined if the device has to be searched by name (optional)
                 searchDeviceDesc=None,   #   -> To be defined if the device has to be searched by description (optional)
                 deviceName='',           #   -> To be defined if deviceId = None and searchDeviceName = None and searchDeviceDesc = None
                 deviceDesc=None,         #   -> To be defined if deviceId = None and searchDeviceName = None and searchDeviceDesc = None

                 testTypeId=None,         # (optional)
                 searchTestTypeName=None, #   -> To be defined if the test type has to be searched by name (optional)
                 searchTestTypeDesc=None, #   -> To be defined if the test type has to be searched by desc (optional)
                 testTypeName='',         #   -> To be defined if testTypeId = None and searchTestTypeName = None and searchTestTypeDesc = None
                 testTypeDesc=None,       #   -> To be defined if testTypeId = None and searchTestTypeName = None and searchTestTypeDesc = None

                 **kwargs):

        ''' Open connection '''
        if testRes is None:
            if host is None:
                raise TestResException("Host has to be defined when testId is not set")
            if username is None:
                raise TestResException("Username has to be defined when testId is not set")
            if password is None:
                raise TestResException("password has to be defined when testId is not set")
            if port is None:
                raise TestResException("port has to be defined when testId is not set")
            if dbname is None:
                raise TestResException("dbname has to be defined when testId is not set")

            testRes = TestResLib(host,username,password,port,dbname)

        ''' Use or create deviceId '''
        if deviceId is None:
            if searchDeviceName is not None:
                deviceIdList = testRes.getDevice(deviceName = searchDeviceName)
                if len(deviceIdList) > 1:
                    raise TestResException("More than one device has been found with name '{}': {}".format(searchDeviceName, deviceIdList))
                deviceId = deviceIdList[0]['deviceId']

            elif searchDeviceDesc is not None:
                deviceIdList = testRes.getDevice(deviceDesc = searchDeviceDesc)
                if len(deviceIdList) > 1:
                    raise TestResException("More than one device has been found with description '{}': {}".format(searchDeviceDesc, deviceIdList))
                deviceId = deviceIdList[0]['deviceId']

            else:
                if deviceDesc is None:
                    raise TestResException("deviceDesc shall be defined when neither deviceId nor searchDeviceName nor searchDeviceDesc are set")
                deviceId = testRes.addDevice(deviceName = deviceName, deviceDesc = deviceDesc)

        ''' Use or create testId '''
        if testId is None:
            if testDesc is None:
                raise TestResException("testDesc shall be defined when testId is not set")
            testId = testRes.addTest(deviceId = deviceId, testDescription = testDesc)

        ''' Use or create testTypeId '''
        if testTypeId is None:
            if searchTestTypeName is not None:
                testTypeIdList = testRes.getTestType(testTypeName = searchTestTypeName)
                if len(testTypeIdList) > 1:
                    raise TestResException("More than one testType has been found with name '{}': {}".format(searchTestTypeName, testTypeIdList))
                testTypeId = testTypeIdList[0]['testTypeId']

            elif searchTestTypeDesc is not None:
                testTypeIdList = testRes.getTestType(testTypeDesc = searchTestTypeDesc)
                if len(testTypeIdList) > 1:
                    raise TestResException("More than one testType has been found with description '{}': {}".format(searchTestTypeDesc, testTypeIdList))
                testTypeId = testTypeIdList[0]['testTypeId']

            else:
                if testTypeDesc is None:
                    raise TestResException("testTypeDesc shall be defined when neither testTypeId nor searchTestTypeName nor searchTestTypeDesc are set")
                testTypeId = testRes.addTestType(testTypeName = testTypeName, testTypeDesc = testTypeDesc)

        t0 = time.time()
        result = self.test_function(**kwargs)
        result['summary']['execution_time'] = (time.time()-t0)

        if result is None:
            raise TestResException("Test routine HAS TO return dictionary containing the test results.")

        testRes.pushSubTest(
                testId = testId,
                testTypeId = testTypeId,
                deviceId = deviceId,
                name = result['name'],
                summary = result['summary'],
                details = result['details'],
                measurements = result['measurements'],
                passed = result['passed']
            )

        return (True if result['passed'] == testRes.SUCCESS else False)